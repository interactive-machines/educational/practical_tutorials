# Practical Tutorials

This repository contains Python notebooks explaining techniques and algorithms that we frequently encounter when working with interactive systems like robots.
The code builds on books and educational materials by others, as acknowledged in each of the Jupyter Notebooks within this repository.

## Setup Environment

Install dependencies with pip. We recommend that you do this within a virtual environment.

```bash
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```

Once the dependencies are installed, run [Jupyter Notebook](https://jupyter.org/):

```bash
(venv) $ jupyter notebook
```

## Tutorials

Below is a list of the tutorials in this repository. All notebooks are within the notebooks directory. The data they require/use is within the data folder.

- **FourierTransform.ipynb** - Discrete Fourier Transforms
- **SequentialBayes.ipynb** - Bayes filters with static state
